#!/usr/bin/env python
import torch
import argparse
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn
from torch.utils.data.sampler import SubsetRandomSampler
from tqdm import tqdm
import time
import numpy as np, random
from PIL import Image
import os
import torch.utils.data as data
from skimage import io, transform, img_as_float
import PIL
import random
import pickle
from SemProj.Important import ResNet34, DataSet, ResNet18
import SemProj.randImGen as randImGen


our_transformation = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.3403, 0.3121, 0.3214),
                         (0.2724, 0.2608, 0.2669))
])
tr = transforms.ToPILImage()
BATCH_SIZE = 100
S0_Card = 1032
LEARNING_RATE = 1e-3
classesNumber = 43 
lmbda=0.1
ITERATIONS = 1
EPOCHS = 1
device='cpu'
#Initial dataset
training_file = "./SemProj/data_pickle/train.p"
with open(training_file, mode='rb') as f:
    train = pickle.load(f)
X_train, y_train = train['features'], train['labels']
trainingdataset = DataSet(X_train,y_train,our_transformation)
elements_idx = list(range(len(trainingdataset)))
state = (np.random.RandomState()).shuffle(elements_idx)
chosen_elements_idx= elements_idx[:1032]
sampler = SubsetRandomSampler(chosen_elements_idx)
trainingDataLoader = torch.utils.data.DataLoader(trainingdataset,batch_size=len(trainingdataset), sampler=sampler,shuffle=False)
for images, labels in trainingDataLoader:
    images = images.permute(0,2,3,1).numpy()
    labels = labels.numpy()
dic = {'features': images, 'labels':labels}
with open('./SemProj/data_pickle/S0.p', 'wb') as handle:
    pickle.dump(dic, handle, protocol=pickle.HIGHEST_PROTOCOL)
    print("Done creating the initial dataset!")