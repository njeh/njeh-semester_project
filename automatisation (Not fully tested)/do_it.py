#!/usr/bin/env python
import torch
import argparse
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn
from torch.utils.data.sampler import SubsetRandomSampler
from tqdm import tqdm
import time
import numpy as np, random
from PIL import Image
import os
import torch.utils.data as data
from skimage import io, transform, img_as_float
import PIL
import random
import pickle
from SemProj.Important import ResNet34, DataSet, ResNet18
import SemProj.randImGen as randImGen


our_transformation = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.3403, 0.3121, 0.3214),
                         (0.2724, 0.2608, 0.2669))
])
tr = transforms.ToPILImage()
BATCH_SIZE = 100
LEARNING_RATE = 1e-3
classesNumber = 43 
lmbda=0.1
ITERATIONS = 1
EPOCHS = 1
device = 'cpu'
def train_model(trainloader,epochs=1,file_name="S0.p", model=None):
    if model==None:
        model = ResNet34()
        model.to(device)
    loss_function=nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=LEARNING_RATE)

    for epoch in tqdm(range(epochs)):
        total_loss = 0
        for batch_idx, (images, labels) in enumerate(trainloader):
            images=images.to(device)
            labels=labels.to(device)
            optimizer.zero_grad() 
            preds = model(images)
            loss = loss_function(preds,labels)
            loss.backward() 
            optimizer.step() 
            total_loss += loss.item()
        del images
        del labels
    return model
def augment(trainingDataLoader,file_name,model=None,model2=None):
    for images,labels in trainingDataLoader:
        images = images.permute(0,2,3,1) 
        images = images.permute(0,3,1,2).numpy()
        integer = int(np.ceil(len(images)))
        images = images[:int(len(images))]
        new_images = np.vstack([images, images])
        for idx, data in enumerate(images):
            data = torch.tensor(data)
            data = data.unsqueeze(0)
            data = data.numpy()
            list_derivatives = []
            data = torch.from_numpy(data).to(device)
            data = Variable(data, requires_grad=True, volatile=False)
            for class_idx in range(classesNumber):
                pred = model(data)[:, class_idx]
                pred.backward()
                list_derivatives.append(data.grad.data.cpu().numpy())
                data.grad.data.zero_()
            grads = list_derivatives
            grad = grads[labels[idx]]
            grad_val = np.sign(grad)
            new_images[len(images)+idx] = new_images[idx] + lmbda * grad_val
        images = new_images
        preds = model2(torch.tensor(images).to(device))
        labels = np.argmax(preds.data.cpu().numpy(), axis=1)
        images = torch.tensor(images).permute(0,2,3,1).numpy()
        dic = {'features': images, 'labels':labels}
        with open('./SemProj/data_pickle/'+file_name, 'wb') as handle:
            pickle.dump(dic, handle, protocol=pickle.HIGHEST_PROTOCOL)
            print("done from pickle")
    return 0

def main():
    argparser = argparse.ArgumentParser(description="Train substitute_model")
    argparser.add_argument("-training_file", metavar='training_file', type=str, help='file to use in the training', default=0)
    argparser.add_argument("-first", metavar='first', type=int, help='first iteration', default=0)
    args = argparser.parse_args()
    substitute_model = ResNet18()
    if(args.first==1):
        model_loader = torch.load('./substitute_model_20',map_location=torch.device('cpu'))
        substitute_model.load_state_dict(model_loader['model_state_dict'])
    substitute_model = substitute_model.to(device)
    train_file = args.training_file
    training_file = "./SemProj/data_pickle/"+str(train_file)
    with open(training_file, mode='rb') as f:
        train = pickle.load(f)
    X_train, y_train = train['features'], train['labels']
    trainingdataset = DataSet(X_train,y_train,our_transformation)
    trainingDataLoader = torch.utils.data.DataLoader(trainingdataset,batch_size=16,shuffle=True)
    test(substitute_model,testingDataLoader)
    if(training_file == "S0.p"):
        substitute_model = train_model(trainingDataLoader,1,train_file,substitute_model)
    else:
        substitute_model = train_model(trainingDataLoader,3,train_file,substitute_model)
    testing_file = "./SemProj/data_pickle/test.p"
    with open(testing_file, mode='rb') as f:
        test = pickle.load(f)
    X_test, y_test = test['features'], test['labels']

    testingdataset = DataSet(X_test,y_test,our_transformation)
    testingDataLoader = torch.utils.data.DataLoader(testingdataset, batch_size=100, shuffle=False)
    BB  = ResNet34()
    BB_loader = torch.load('./BlackBox',map_location=torch.device('cpu'))
    BB.load_state_dict(BB_loader['model_state_dict'])
    BB.to(device)
    def test(model, test_loader):
        correct = 0
        total = 0
        with torch.no_grad():
            for data in test_loader:
                images, labels = data[0].to(device), data[1].to(device)
                outputs = model(images)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()
        return 100 * correct / total

    val1 = test(substitute_model,testingDataLoader)    
    if(training_file == "S0.p"):
        substitute_model = train_model(trainingDataLoader,1,train_file,substitute_model)
    else:
        substitute_model = train_model(trainingDataLoader,3,train_file,substitute_model)
    val 2 = test(substitute_model,testingDataLoader)
    if(val1<=val2): #it would be better to define a threshold to gain more time!
        torch.save({'model_state_dict':substitute_model.state_dict(),},'./substitute_model_20')
        trainingDataLoader = torch.utils.data.DataLoader(trainingdataset,batch_size=len(trainingdataset),shuffle=True)
        augment(trainingDataLoader,train_file,substitute_model,BB)
    else:
        open_file = open("waiting_list.p", "rb")
        waiting_list = pickle.load(open_file)
        open_file.close()
        waiting_list.remove(train_file)
        open_file = open("waiting_list.p", "wb")
        pickle.dump(waiting_list, open_file)
        open_file.close()
if __name__ == "__main__":
    main ()
    print("Done, check your output file!")