#!/usr/bin/env python
import argparse
import numpy as np
import os
import pickle
def Init_vect(file_name=None,x=None,y=None)
'''
This function takes as input a pickle file and divide in two halves.
'''
    if(file_name != None):
	    initial_file = "./SemProj/data_pickle/"+file_name
	    with open(initial_file, mode='rb') as f:
	        file = pickle.load(f)
	    X, y = file['features'], file['labels']
	    tail = len(X)
	    X1 = X[:np.ceil(tail/2)]
	    X2 = X[np.ceil(tail/2):]
	    y1 = y[:np.ceil(tail/2)]
	    y2 = y[np.ceil(tail/2):]
    else:
        tail = len(x)
        X1 = x[:np.ceil(tail/2)]
        X2 = x[np.ceil(tail/2):]
        y1 = y[:np.ceil(tail/2)]
        y2 = y[np.ceil(tail/2):]
    return X1, y1, X2, y2



def Init(file_name):
'''
This method is what you need to customize to implement a different block slicing method.
'''
    waiting_list = []
    x,y,z,w = Init_vect(file_name = file_name)
    x11,y11,x12,y12 = Init_vect(x= x,y=y)
    x21,y21,x22,y22 = Init_vect(x=z,y=w)
    waiting_list =  pick_dump(x11,y11,x12,y12,'first_',waiting_list)
    waiting_list = pick_dump(x21,y21,x22,y22,'second_',waiting_list)
    return waiting_list


def pick_dump(x,y,z,w,file_name,waiting_list):
    dic = {'features': x, 'labels':y}
    with open('./SemProj/data_pickle/'+file_name+'1.p', 'wb') as handle:
        pickle.dump(dic, handle, protocol=pickle.HIGHEST_PROTOCOL)
        waiting_list.append(file_name+'1.p')
    dic = {'features': z, 'labels':w}
    with open('./SemProj/data_pickle/'+file_name+'2.p', 'wb') as handle:
        pickle.dump(dic, handle, protocol=pickle.HIGHEST_PROTOCOL)
        waiting_list.append(file_name+'2.p')
    return waiting_list

def main():
	argparser = argparse.ArgumentParser(description="Heuristics customization")
    argparser.add_argument("-training_file", metavar='training_file', type=str, help='file to use in the training', default=0)
    argparser.add_argument("-first",metavar="'yes' if called for the first time, otherwise 'no'",type=str,help="yes/no value",default="yes")
    args = argparser.parse_args()
    if(args.first == "yes"):
    	waiting_list = []
    else:
        open_file = open("waiting_list.p", "rb")
        waiting_list = pickle.load(open_file)
        open_file.close()
    waiting_list = Init(args.training_file)
    open_file = open("waiting_list.p", "wb")
    pickle.dump(waiting_list, open_file)
    open_file.close()


if __name__ == "__main__":
    main ()
    print("Done from Init")
