#!/usr/bin/env python
import torch
import argparse
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn
from torch.utils.data.sampler import SubsetRandomSampler
from tqdm import tqdm
import time
import numpy as np, random
from PIL import Image
import os
import torch.utils.data as data
from skimage import io, transform, img_as_float
import PIL
import random
import pickle
from SemProj.Important import ResNet34, DataSet, ResNet18


our_transformation = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.3403, 0.3121, 0.3214),
                         (0.2724, 0.2608, 0.2669))
])
tr = transforms.ToPILImage()
BATCH_SIZE = 16
S0_Card = 24*43
LEARNING_RATE = 1e-3
classesNumber = 43 
lmbda=0.1
ITERATIONS = 1
EPOCHS = 1


def test_model(model, test_loader):
    correct = 0
    total = 0
    with torch.no_grad():
        for data in test_loader:
            images, labels = data[0].to(device), data[1].to(device)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
        print(correct)
    return 100 * correct / total

device = 'cuda' if torch.cuda.is_available() else 'cpu'

BB  = ResNet34()
BB_loader = torch.load('./BlackBox',map_location=torch.device('cpu'))
BB.load_state_dict(BB_loader['model_state_dict'])
BB.to(device)

testing_file = "./SemProj/data_pickle/test.p"
with open(testing_file, mode='rb') as f:
    test = pickle.load(f)
X_test, y_test = test['features'], test['labels']

def main():
    argparser = argparse.ArgumentParser(description="Train substitute_model")
    argparser.add_argument("-assertt", metavar='assertt', type=int, help='assertion', default=0)
    args = argparser.parse_args()
    if(args.assertt==0):
    	training_file = "./SemProj/data_pickle/train.p"
    else:
    	training_file = "./SemProj/data_pickle/train_subs_1.p"
    with open(training_file, mode='rb') as f:
        train = pickle.load(f)
    X_train, y_train = train['features'], train['labels']
    testingdataset = DataSet(X_test,y_test,our_transformation)
    testingDataLoader = torch.utils.data.DataLoader(testingdataset, batch_size=BATCH_SIZE, shuffle=False)
    trainingdataset = DataSet(X_train,y_train,our_transformation)
    elements_idx = list(range(len(trainingdataset)))
    state = (np.random.RandomState()).shuffle(elements_idx)
    print(len(trainingdataset))
    if(args.assertt==0):
        chosen_elements_idx= elements_idx[:S0_Card]
    else:
        chosen_elements_idx= elements_idx[:len(trainingdataset)]
    sampler = SubsetRandomSampler(chosen_elements_idx)
    trainingDataLoader = torch.utils.data.DataLoader(trainingdataset,batch_size=len(trainingdataset), sampler=sampler,shuffle=False)
    substitute_model = ResNet18()
    if(args.assertt!=0):
        model_loader = torch.load('./substitute_model',map_location=torch.device('cpu'))
        substitute_model.load_state_dict(model_loader['model_state_dict'])
    substitute_model = substitute_model.to(device)
    for images, labels in trainingDataLoader:
        images = images.permute(0,2,3,1).numpy()
        #print(np.shape(images))
        labels = labels.numpy()
        print("Done extracting Images and Labels :)")
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(substitute_model.parameters(), lr=LEARNING_RATE)
    state = np.random.RandomState()
    for epochs in range(EPOCHS):
        images = torch.tensor(images).permute(0,3,1,2).numpy()
        integer = int(np.ceil(len(images)/2))
        images = images[:int(len(images)/2)]
        new_images = np.vstack([images, images])
        for idx, data in enumerate(images):
            data = torch.tensor(data)
            data = data.unsqueeze(0)
            data = data.numpy()
            list_derivatives = []
            data = torch.from_numpy(data).to(device)
            data = Variable(data, requires_grad=True, volatile=False)
            for class_idx in range(classesNumber):
                pred = substitute_model(data)[:, class_idx]
                pred.backward()
                list_derivatives.append(data.grad.data.to(device).numpy())
                data.grad.data.zero_()
            grads = list_derivatives
            grad = grads[labels[idx]]
            grad_val = np.sign(grad)
            new_images[len(images)+idx] = new_images[idx] + lmbda * grad_val
        images = new_images
        preds = BB(torch.tensor(images).to(device))
        labels = np.argmax(preds.data.to(device).numpy(), axis=1)
        images = torch.tensor(images).permute(0,2,3,1).numpy()
    dic = {'features': images, 'labels':labels}
    torch.save({'model_state_dict':substitute_model.state_dict(),},'./substitute_model')
    with open('./SemProj/data_pickle/train_subs_1.p', 'wb') as handle:
        pickle.dump(dic, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print(np.shape(images))
        print("done from pickle")

if __name__ == "__main__":
    main ()
    print("Done, check your output file!")



